var globalChatterId = '';
var globalChatterName = '';
function showChatListScreen() {
    
    globalChatterId = '';
    
    $('#listofUser').html();
    $.ajax({ url: rootUrl + "/users/assigned_clients/" + globalUserId,
        dataType: 'json',
        success: function(data) {
           
        	console.log(data.clients);
        	console.log(data.unreads);

           	html = loadDataTemplate('#chatterItem', data.clients);
           	$('#listofUser').html(html);
        }
    });
    
    hideOtherScreens();
    $('#roomListScreen').show();
}

function selectChatPartner(id, name) {

    socket.emit('change room', { from: globalChatterId, to: id, user_id: globalUserId });
    globalChatterId = id;
    globalChatterName = name;
    
    hideOtherScreens();
    
    $('.msg_container').remove();
    $('#chatListScreen').show();
    $('#chatInputEl').show();
}


var sendAMessage = function(val) {
            
    socket.emit('chat message', { user_id: globalUserId, message: val, room: globalChatterId });
            
    var html = loadDataTemplate('#messageSentItem', {message: val, name: 'You', time: ''});
    $('.msg_container_base').append(html);
}

$('#sendAMessage').click(function(){
    
    sendAMessage($('#chatInputEl input').val());
    $('#chatInputEl input').val('');
});

$('#chatInputEl input').keypress(function(e) {
    
    if (e.which == 13) {
        
        sendAMessage($(this).val());
        $(this).val('');
        return false;
    }
});