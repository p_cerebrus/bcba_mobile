/*$(document).ready(function () {
	$('#btnLogin').click(function() {
                                         
		$.ajax({
			method: "POST",
			url: rootUrl + "/login",
			data: { username: $('#user').val(), password: $('#password').val() }
		})
		.done(function( msg ) {
                                               
			$('#user').val('');
			$('#password').val('');
			$('#loginView').hide();
                                               
			globalUserId = msg.user_id;
			window.localStorage.setItem("globalUserId", globalUserId);
			window.localStorage.setItem("loggedTime", +new Date);
			
			loadSessionInfo(function() {
                                                               
				$('#page').show();
				$('#mainMenu').show(); 
			});
		})
		.error(function( msg ) {
    
			alert('Username or password is incorrect!');
		});
	});
});*/

angular.module('app', ['ionic', 'ngRoute'])
.controller('loginCtrl', ['$scope', '$http', function ($scope, $http){
	alert("controller loaded");
	$scope.login = function(){
		$http.post('/login', {username: $scope.username, password: $scope.password})
			.success(function (){
				alert("login success");
			})
			.error(function (msg){
				alert("login fail");
				alert(JSON.stringify(msg));
			});
	}
}]);
