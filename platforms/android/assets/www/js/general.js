 function loadDataTemplate(selector, data) {
    	
	var source   = $(selector).html();
	var template = Handlebars.compile(source);
	var html     = template(data);
	return html;
}

function hideOtherScreens() {

    $('.ui-screen').hide();
	$('#confirmsessionBtn').hide();
    $('#chatInputEl').hide();
    
    if (socket)
    	socket.emit("leave all rooms");
}

$.fn.bindWithDelay = function( type, data, fn, timeout, throttle ) {

    if ( $.isFunction( data ) ) {
        throttle = timeout;
        timeout = fn;
        fn = data;
        data = undefined;
    }

    // Allow delayed function to be removed with fn in unbind function
    fn.guid = fn.guid || ($.guid && $.guid++);

    // Bind each separately so that each element has its own delay
    return this.each(function() {

        var wait = null;

        function cb() {
            var e = $.extend(true, { }, arguments[0]);
            var ctx = this;
            var throttler = function() {
                wait = null;
                fn.apply(ctx, [e]);
            };

            if (!throttle) { clearTimeout(wait); wait = null; }
            if (!wait) { wait = setTimeout(throttler, timeout); }
        }

        cb.guid = fn.guid;

        $(this).bind(type, data, cb);
    });
};

function logout() {
	
	globalUserId = '';
	
	window.localStorage.removeItem("globalUserId");
	window.localStorage.removeItem("loggedTime");

	$('#loginView').show();
	$('#page').hide();
    $('#chatInputEl').hide();
}

$(document).ready(function(){
	$('#searchCardValue').bindWithDelay("keyup", function() {
				
		if ($('#searchCardValue').val()) {

			$('.sheet').each(function() {

				var el = $(this);
				
				if (el.data('name').indexOf($('#searchCardValue').val()) == -1)
					el.hide();
				else
					el.show();
			});
		} else {
			
			$('.sheet').show();
		}
	}, 700, true);


	$('.selected-channel').hide();

	socket = io(rootUrl);

	socket.on('connect', function() {
	    
		socket.on('chat message', function(data){
	    	
			console.log(data);
	    	if (data.room == globalChatterId && data.message) {

	            html = loadDataTemplate('#messageReceiveItem', {message: data.message, name: globalChatterName, time: ''});
	            $('.msg_container_base').append(html);
	    	}
	    });
	    
	    socket.on('room message history', function(messages){

			console.log(messages);
	        for (var i=0; i<messages.length; i++){
	        	
	            var html = loadDataTemplate('#messageReceiveItem', {message: messages[i].message, name: messages[i].username, time: ''});
	            $('.msg_container_base').append(html);
	        }
	    });
	});
	$('#btnChangeClient').click(function(){
	    	
    	if (globalClient.length > 1) {
    		
    	}
    });
});