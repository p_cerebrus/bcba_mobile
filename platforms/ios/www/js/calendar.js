var calendarConfiguration = 1;
	function showCalendarWidget() {
		
		if ($('#switchCalendar button').html() == 'Loading') return;
		
		calendarConfiguration++;
		if (calendarConfiguration > 3) calendarConfiguration = 1;
		
		showCalendarScreen();
	}
	
	function showCalendarScreen() {
		
		$('#switchCalendar button').html('Loading');
		
		var $param = 'daily';
		switch(calendarConfiguration) {
	    	case 2:
	    		$param = 'weekly';
	        	break;
	    	case 3:
	    		$param = 'biweekly';
	        	break;
	    	case 4:
	    		$param = 'monthly';
	        	break;
		}
		
		// remove all existing calendar
		$('.calendar').remove();
		
		// fetch new one
        $.ajax({
			method: "GET",
			url: rootUrl + "/sessions/calendar/" + globalUserId + "/" + $param,
		})
		.done(function( data ) {
              
            var sindex = 0;
			$.each(data, function(i, item){

				$.each(item.array_agg, function(i, item){
    				
					$.get( rootUrl + "/sessions/summary/" + item, function( data ) {
						
						$.each(data, function(i, item){

							var startDate = '';
							if (item.schedule_in)
								startDate = $.format.date(new Date(item.schedule_in), 'hh:mm');
							
							var endDate = '';
							if (item.schedule_out)
								endDate = $.format.date(new Date(item.schedule_out), 'hh:mm');
							
							var card = {
								index: ++sindex,
								id: item.client_id,
								clientName: "",
								"in": startDate,
								"out": endDate,
								itt: null
							};

							$.ajax({ url: rootUrl + "/clients/client/" + card.id, 
								async: false,
								dataType: 'json',
								success: function(msg) {
				    				
									card.clientName = msg.firstname + ' ' + msg.lastname;
								}
							});

							$.each(item.itt, function(i, item){
			    				
								var itts = new Array();
								$.ajax({ url: rootUrl + "/lesson/itt/" + item, 
									async: false,
									dataType: 'json',
									success: function(data) {
					    				
										$.each(data, function(i, item){
											
											itts.push({
												ittId: item.lesson_id,
												name: item.name
											});
										});
									}
								});

								card.itt = itts;
			    	        });
							
	                        html = loadDataTemplate('#calendarItem', card);
	                        $('#switchCalendar').after(html);
		    	        });
						
						switch(calendarConfiguration) {
				    		case 1:
								$('#switchCalendar button').html('Daily data');
				        		break;
					    	case 2:
								$('#switchCalendar button').html('Weekly data');
					        	break;
					    	case 3:
								$('#switchCalendar button').html('Biweekly data');
					        	break;
					    	case 4:
								$('#switchCalendar button').html('Monthly data');
					        	break;
						}
					});
    	        });
	        });
		});
		
		hideOtherScreens();
		$('#calendarDataScreen').show();
	}