// ----------------------Daily Probe Review-----------------
	var globalClients;
    function loadSessionInfo(callback) {
        
        // decorator trick
        $('.header').addClass('bar').addClass('bar-header');
        
        $.ajax({
               method: "GET",
               url: rootUrl + "/sessions/session/" + globalUserId,
        })
        .done(function( msg ) {
        	
			globalClients = msg;
        	
			if(msg && msg.length > 0) {
              
                globalSessionId = msg[0].id;
                globalClientId = msg[0].client_id;
              
                $('#sessionStart').html(msg[0].schedule_in);
                $('#sessionEnd').html(msg[0].schedule_out);
              
                $.ajax({
                     method: "GET",
                     url: rootUrl + "/clients/client/" + globalClientId,
                })
                .done(function( msg ) {
                    
                    $('#loggerName').html(msg.firstname + ' ' + msg.lastname);
                    $('#loggerAddress').html(msg.address);
                 });
              
                var index = 0;

                if (msg[0].itt) {

                    $.each(msg[0].itt, function(key, value) {
                         
                         $.ajax({
                                method: "GET",
                                url: rootUrl + "/lesson/itt/" + value,
                                })
                         .done(function( data ) {
                               
                               console.log(data);
                               index++;
                               var lessons = {
                                index: index,
                                lessons: data
                               }
                               
                               var html = loadDataTemplate('#lessonDetailsItem', lessons);
                               $('#ittTable').html(html);
                               
                               html = loadDataTemplate('#dailySheetItem', data);
                               $('#searchCard').after(html);
    					});
                    });
                }
              
                // hide change client button if there is more than 1 client
                if (msg.length > 1)
                	$('#btnChangeClient').show();
                else
                	$('#btnChangeClient').hide();
                
                $('#generalInfo').show();
                $('#confirmsessionBtn').show();
			} else {
              
				$('#confirmsessionBtn').hide();
			}

              loadAntecedents();
              
              if (callback)
            	  callback();
        });
    }

	function loadSummaryProbe() {

		$('#confirmsessionBtn').show();
		$('#dailyReviewScreen').show();
		$('#summaryTable').find('tbody').empty();
		
		var data = new Array();
		$('.sheet').each(function() {

			var el = $(this);
			
			var $class = 'green-background';
			if (el.data('type') == 'retention')
				$class = 'red-background';
			else if (el.data('type') == 'cold')
				$class = 'yellow-background';
			
			data.push({
				id: el.data('id'),
				category: el.data('cat'),
				name: el.data('name'),
				type: el.data('type'),
				target_id: el.data('target'),
				class: $class
			});
		});
		
		console.log(data);
		var html = loadDataTemplate('#dailyProbeItem', data);
		$('#summaryTable').find('tbody').html(html);
    }

	var sessionStep = 1;
	function confirmSession(){
		
		sessionStep++;
		if (sessionStep == 2) {
			
			hideOtherScreens();
			$('#confirmsessionBtn').show();
			$('#confirmsessionBtn').find('div').html('CONTINUE');
			$('#confirmsessionBtn').removeClass('green-background').addClass('blue-background');
			$('#dailyDataScreen').show();
		} else if (sessionStep == 3) {
			
			var cont = true;
			$('.sheet').each(function() {
				
				var el = $(this);
				if (!el.find('.background-checked').html() && !el.find('.background-unchecked').html() && el.data('type') != 'known') {

					cont = false;
				}
			});

			if (!cont) {
				
				alert('Please select all probe before continue');
				sessionStep--;
			} else {

				$('#confirmsessionBtn').find('div').html('SUBMIT');
				$('#confirmsessionBtn').removeClass('blue-background').addClass('green-background');

				hideOtherScreens();
				loadSummaryProbe();
			}
		} else if (sessionStep == 4) {

			$('#confirmsessionBtn').find('div').html('CONTINUE');

			hideOtherScreens();
			endProbes();
		}
	}
	
	function endProbes() {
		
		sessionStep = 1;
		
		$('.endProbe').each(function() {

			$.ajax({
	  	        method: "GET",
	  	        url: rootUrl + "/sessions/end_probe/" + $(this).data('index'),
	  	     })
	  	     .done(function( data ) {});
		});
		
		$(document).ajaxStop(function () {

			$.ajax({
	  	        method: "GET",
	  	        url: rootUrl + "/sessions/end/" +globalSessionId,
	  	     })
	  	     .done(function( data ) {});
		});
	}
	
	function noPress(el) {
		
		el = $(el);
		var parent = el.parents('.sheet');
		var attemp = parseInt(parent.data('attempt'));

		// baseline 3 attempts
		if (parent.data('type') == 'baseline' && attemp >= 3) return;
		else if (parent.data('type') != 'baseline' && attemp >= 1) return;

		if (el.hasClass('background-uncheck') && parent.data('type') != 'known') {

			parent.find('.addCommentSection').show();
			
			// reset to original state
			if (parent.find('.background-checked').html())
				parent.find('.background-checked').removeClass("background-checked").addClass("background-check");
			
			el.removeClass("background-uncheck").addClass("background-unchecked");
			
			attemp++;
			parent.data('attempt', attemp);
			parent.find('.attempText').html(attemp);

			$.ajax({
	            method: "POST",
	            url: rootUrl + "/lesson/no_press/" + parent.data('id'),
	            data: {lesson_id: parent.data('id'), category: parent.data('cat'), count: attemp, type: parent.data('type'), therapist_id: globalUserId, client_id: globalClientId}
	         })
		     .done(function( msg ) {

		    	 console.log(msg);
		     });
		}
	}

	function yesPress(el) {
		
		el = $(el);
		var parent = el.parents('.sheet');
		var attemp = parseInt(parent.data('attempt'));

		// baseline 3 attempts
		if (parent.data('type') == 'baseline' && attemp >= 3) return;
		else if (parent.data('type') != 'baseline' && attemp >= 1) return; 
		
		if (el.hasClass('background-check') && parent.data('type') != 'known') {

			parent.find('.addCommentSection').hide();
			
			if (parent.find('.background-unchecked').html())
				parent.find('.background-unchecked').removeClass("background-unchecked").addClass("background-uncheck");
			
			el.removeClass("background-check").addClass("background-checked");
			
			attemp++;
			parent.data('attempt', attemp);
			parent.find('.attempText').html(attemp);
			
			$.ajax({
	            method: "POST",
	            url: rootUrl + "/lesson/yes_press/" + parent.data('id'),
	            data: {lesson_id: parent.data('id'), category: parent.data('cat'), count: attemp, type: parent.data('type'), therapist_id: globalUserId, client_id: globalClientId}
	        })
		    .done(function( msg ) {

		     	console.log(msg);
		    });
		}
	}
	
	function addCardComment(e) {

		var parent = $(e).parents('.sheet');
		var comment = prompt("Please enter your comment", "");
		if (comment != null) {

			$.ajax({
	            method: "POST",
	            url: rootUrl + " /lesson/message",
	            data: {lesson_id: parent.data('id'), message: comment}
	        })
		    .done(function( msg ) {

		     	console.log(msg);
		    });
		}
	}