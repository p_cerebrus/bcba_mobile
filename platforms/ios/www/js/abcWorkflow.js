var aData = new Array();
    var bData = new Array();
    var cData = new Array();
    
    function chooseABC(data) {
    	
    	if (currentProcess == 1)
    		aData.push(data);
		else if (currentProcess == 2)
    		bData.push(data);
		else if (currentProcess == 3)
    		cData.push(data);
    }

	function submitABC() {

		$.each(aData, function( index, value ) {
			
			 $.ajax({
	            method: "POST",
	            url: rootUrl + "/behaviors/antecedent",
	            data: { id: value, name: 'dummy', client_id: globalClientId }
	         })
		     .done(function( msg ) {
		           
		     });
		 	
			 aData = new Array();
		});

		$.each(bData, function( index, value ) {

	         $.ajax({
	            method: "POST",
	            url: rootUrl + "/behaviors/behaviors",
	            data: { id: value, name: 'dummy', client_id: globalClientId }
	         })
	         .done(function( msg ) {
	                      
	            bData = new Array();
	         });
	         
			 bData = new Array();
		});

		$.each(cData, function( index, value ) {

	         $.ajax({
	            method: "POST",
	            url: rootUrl + "/behaviors/consequence",
	            data: { id: value, name: 'dummy', client_id: globalClientId }
	         })
	         .done(function( msg ) {
	                      
	         });

	         cData = new Array();
		});
                             
         
         $('#abcHeaderBtn').click();
         $('#prepareStep').show();
         $('#finishStep').hide();
         
         currentProcess=1;
         loadAntecedents();
	}
	
	function nextProcess() {
		
		currentProcess++;
		
		if (currentProcess == 2 && aData)
			loadBehaviors();
		else if (currentProcess == 3 && bData) {
			
			loadConsequences();
			$('#prepareStep').hide();
			$('#finishStep').show();
		}
	}

    function loadAntecedents() {
        
        var $container = $('#abcMenu div');
        $container.empty();
        
        // alert(1);
        $('#abcTitle').html('Antecedents');
        $.ajax({
			method: "GET",
			url: rootUrl + "/settings/antecedents?client=" + globalClientId,
		})
		.done(function( data ) {
                     
			// alert(2);
			var html = loadDataTemplate('#abcItem', data);
			$container.html(html);
		});
    }
    
    function loadBehaviors() {
        
        var $container = $('#abcMenu div');
        $container.empty();
        
        $('#abcTitle').html('Behaviors');
        $.ajax({
               method: "GET",
               url: rootUrl + "/settings/behaviors?client=" + globalClientId,
               })
               .done(function( data ) {
               
            	   var html = loadDataTemplate('#abcItem', data);
            	   $container.html(html);
               })
               .error(function( msg ) {
                            
               });
    }
    
    function loadConsequences() {
        
        var $container = $('#abcMenu div');
        $container.empty();

        $('#abcTitle').html('Consequences');
        $.ajax({
			method: "GET",
			url: rootUrl + "/settings/consequences?client=" + globalClientId,
		})
		.done(function( data ) {
            	   
			var html = loadDataTemplate('#abcItem', data);
			$container.html(html);
		})
		.error(function( msg ) {
                            
		});
    }